# RDF/S Deep Reasoner

## Description
This project seeks to train and compare several LSTM networks to learn deductive reasoning according to subsets of RDFS rules.

## Contributors
- Brayden Pankaskie
- Aaron Eberhart
- Pascal Hitzler
