import tensorflow as tf

class DeepModel(tf.keras.Model):
    """A simple Deep Tensorflow model with one input layer and two LSTM layers.  Returns full output from last LSTM layer."""
    def __init__(self, input_shape, num_middle, num_outs):
        super(DeepModel, self).__init__()
        self.layer0 = tf.keras.layers.InputLayer(input_shape=input_shape)
        self.layer1 = tf.keras.layers.LSTM(units=num_middle, return_sequences=True)
        self.layer2 = tf.keras.layers.LSTM(units=num_outs, return_sequences=True)

    def call(self, inputs):
        x = self.layer0(inputs)
        x = self.layer1(x)
        return self.layer2(x)


class FlatModel(tf.keras.Model):
    """A simple LSTM Tensorflow model with one input layer and one LSTM layer.  Returns full output from last LSTM layer."""
    def __init__(self, input_shape, num_outs):
        super(FlatModel, self).__init__()
        self.layer0 = tf.keras.layers.InputLayer(input_shape=input_shape)
        self.layer1 = tf.keras.layers.LSTM(units=num_outs, return_sequences=True)

    def call(self, inputs):
        x = self.layer0(inputs)
        x = self.layer1(x)
        return x