import numpy
import json

def get_rdf_data(file):
    """Gets RDF data from json file specified."""
    with open(file) as f:
        data = json.load(f)
        return data


def pad_kb(_list):
    """Pads a list of lists so that each list within the main list is equal sizes."""
    targetPadNum = 0
    for i in range(len(_list)):
        # Gets the max padding length
        if (len(_list[i]) > targetPadNum):
            targetPadNum = len(_list[i])

    # Pads each list within the main list
    for j in range(len(_list)):
        while len(_list[j]) < targetPadNum:
            _list[j].append(0.0)

    return _list


def find_padding_numbers(_list):
    """Returns padding information for both the number of timesteps and the number of items in each timestep."""
    target_ts_padding = 0
    target_item_padding = 0

    for sample_index in range(len(_list)):
        sample = _list[sample_index]

        # Finds the max number of timesteps
        if len(sample) > target_ts_padding:
            target_ts_padding = len(sample)

        # Gets the max num items contained in any timestep list
        for ts_index in range(len(sample)):
            ts_list = sample[ts_index]
            if (len(ts_list) > target_item_padding):
                target_item_padding = len(ts_list)

    return target_ts_padding, target_item_padding


def pad_list(_list):
    """Pads each sample of output or support data so that all samples will fit into the largest sample and can be
    converted into an array."""

    ts_padding, item_padding = find_padding_numbers(_list)

    for sample in _list:
        for ts in sample:
            if len(ts) < item_padding:
                ts.extend([0] * (item_padding - len(ts)))
        while len(sample) < ts_padding:
            sample.append([0] * item_padding)

    return _list


def convert_data_to_arrays(kb, supp, outs):
    """Takes data and makes sure they are arrays."""
    supp = pad_list(supp)
    outs = pad_list(outs)
    kb = pad_kb(kb)

    KB = numpy.array(kb, dtype=numpy.float64)
    Supp = numpy.zeros((len(supp)), dtype=numpy.ndarray)
    Outs = numpy.zeros((len(outs)), dtype=numpy.ndarray)

    for i in range(len(supp)):
        Supp[i] = numpy.array(supp[i], dtype=numpy.float64)

    for i in range(len(outs)):
        Outs[i] = numpy.array(outs[i], dtype=numpy.float64)

    return KB, Supp, Outs


def combine_datasets(file_list):
    """Combines smaller datasets located by the file_list into one large set.  If data doesn't fit into memory use a different approach."""
    # Load datasets into memory.
    datasets = []
    for file in file_list:
        datasets.append(get_rdf_data(file))

    KB = []
    Supp = []
    Outs = []
    decode_property = 0
    decode_nonproperty = 0

    for dataset in datasets:
        # Combine similar sample lists.
        KB.extend(dataset['kB'])
        Supp.extend(dataset['supports'])
        Outs.extend(dataset['outputs'])

        # Finds largest of both concepts and roles
        if dataset['concepts'] > decode_nonproperty:
            decode_nonproperty = dataset['concepts']
        if dataset['roles'] > decode_property:
            decode_property = dataset['roles']

    KB, Supp, Outs = convert_data_to_arrays(KB, Supp, Outs)

    return KB, Supp, Outs, decode_nonproperty, decode_property
