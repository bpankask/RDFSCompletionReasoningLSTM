from unittest import TestCase

class DataManipulationTests(TestCase):

    def test_pad_kb__everyone_same_size(self):
        from dataset_manipulation import get_rdf_data, pad_kb
        kb = [
                [1, 2, 3],
                [1, 2, 3, 4, 5, 6, 7],
                [1, 2, 3, 4, 5, 6],
                [1, 2, 3, 4],
                [1, 2, 3, 4, 5],
                [1, 5, 3, 6, 3],
                [1, 2],
                [1, 8, 4, 9, 5, 4, 8, 9, 6, 4, 7]
             ]
        kb = pad_kb(kb)

        for sample in kb:
            self.assertEqual(len(kb[0]), len(sample))

    def test_pad_kb__removing_padding_reveals_original(self):
        from dataset_manipulation import pad_kb

        kb = [
                [1, 2, 3],
                [1, 2, 3, 4, 5, 6, 7],
                [1, 2, 3, 4, 5, 6],
                [1, 2, 3, 4],
                [1, 2, 3, 4, 5]
             ]

        KB = pad_kb(kb)

        for sample in KB:
            for index in range(len(sample)):
                if sample[index] == 0.0:
                    del sample[index:]
                    break
        self.assertEqual(kb, KB)

    def test_convert_data_to_arrays__shape_preserved(self):
        from dataset_manipulation import convert_data_to_arrays, get_rdf_data
        data = get_rdf_data('test_data/dublin_core_2012_6.json')
        kb, supp, outs, numConcepts, numRoles = data['kB'], data['supports'], data['outputs'], data['concepts'], data[
            'roles']
        KB, supports, outputs = convert_data_to_arrays(data['kB'], data['supports'], data['outputs'])

        self.assertEqual(len(kb), len(KB))
        self.assertEqual(len(supp), len(supports))
        self.assertEqual(len(outs), len(outputs))

        for sample in range(len(kb)):
            self.assertEqual(len(supp[sample]), len(supports[sample]))
            self.assertEqual(len(outs[sample]), len(outputs[sample]))
            self.assertEqual(len(outputs[sample]), len(supports[sample]))

    def test_pad_list__each_sample_len_and_ts_len_are_correct(self):
        from dataset_manipulation import pad_list

        data = [
                [
                    [1, 1, 1],
                    [2, 2, 2, 2, 2],
                    [3, 3]
                ],
                [
                    [1, 1, 1],
                    [2, 2, 2, 2, 2],
                    [3]
                ],
                [
                    [1, 1, 1, 1, 1, 1, 1],
                    [2, 2, 2, 2, 2],
                    [3, 3, 3],
                    [4, 4, 4, 4]
                ]
            ]
        sample_len = 4
        ts_len = 7
        list = pad_list(data)

        for sample in list:
            self.assertEqual(len(sample), sample_len)
            for ts in sample:
                self.assertEqual(len(ts), ts_len)

    def test_find_padding_numbers__on_support_and_output_data(self):
        from dataset_manipulation import find_padding_numbers

        sample_len = 5
        ts_len = 8

        data = [
            [
                [1, 1, 1],
                [2, 2, 2, 2, 2],
                [3, 3]
            ],
            [
                [1, 1, 1],
                [2, 2, 2, 2, 2],
                [3],
                [4, 4],
                [5, 5, 5, 5]
            ],
            [
                [1, 1, 1],
                [2, 2, 2, 2, 2],
                [3]
            ],
            [
                [1, 1, 1, 1, 1, 1, 1, 1],
                [2, 2, 2, 2, 2],
                [3, 3, 3],
                [4, 4, 4, 4]
            ]
        ]
        s, ts = find_padding_numbers(data)

        self.assertEqual(s, sample_len)
        self.assertEqual(ts, ts_len)

    def test_combine_datasets__correct_len_of_samples(self):
        from dataset_manipulation import combine_datasets, get_rdf_data
        dataset_1 = get_rdf_data('test_data/dbpedia_2015_8.json')
        dataset_2 = get_rdf_data('test_data/dublin_core_2012_6.json')
        dataset_3 = get_rdf_data('test_data/Xenopus anatomy and development_8.json')

        datasets = ['test_data/dbpedia_2015_8.json', 'test_data/dublin_core_2012_6.json', 'test_data/Xenopus anatomy and development_8.json']
        KB, Supp, Outs, decode_nonproperty, decode_property = combine_datasets(datasets)

        kb_len = len(dataset_1['kB']) + len(dataset_2['kB']) + len(dataset_3['kB'])
        supp_len = len(dataset_1['supports']) + len(dataset_2['supports']) + len(dataset_3['supports'])
        outs_len = len(dataset_1['outputs']) + len(dataset_2['outputs']) + len(dataset_3['outputs'])

        self.assertEqual(kb_len, len(KB))
        self.assertEqual(supp_len, len(Supp))
        self.assertEqual(outs_len, len(Outs))

    def test_combine_datasets__finds_max_decode_numbers(self):
        from dataset_manipulation import combine_datasets, get_rdf_data
        dataset_1 = get_rdf_data('test_data/dbpedia_2015_8.json')
        dataset_2 = get_rdf_data('test_data/dublin_core_2012_6.json')
        dataset_3 = get_rdf_data('test_data/schemaorg_7.json')
        dataset_4 = get_rdf_data('test_data/Ontology for Biomedical Investigations_test.json')

        datasets = ['test_data/dbpedia_2015_8.json', 'test_data/dublin_core_2012_6.json',
                    'test_data/Ontology for Biomedical Investigations_test.json', 'test_data/schemaorg_7.json']
        KB, Supp, Outs, decode_nonproperty, decode_property = combine_datasets(datasets)

        self.assertEqual(decode_nonproperty, 76)
        self.assertEqual(decode_property, 9)

    def test_combine_datasets__can_seperate_into_individual_datasets(self):
        '''IMPORTANT: This test only works if you change the combine_datasets method to convert lists to arrays with float64.'''

        from dataset_manipulation import combine_datasets, get_rdf_data
        dataset_1 = get_rdf_data('test_data/dbpedia_2015_8.json')
        dataset_2 = get_rdf_data('test_data/dublin_core_2012_6.json')
        dataset_3 = get_rdf_data('test_data/schemaorg_7.json')

        datasets = ['test_data/dbpedia_2015_8.json', 'test_data/dublin_core_2012_6.json', 'test_data/schemaorg_7.json']
        KB, Supp, Outs, decode_nonproperty, decode_property = combine_datasets(datasets)

        # take away padding
        KB = KB.tolist()
        for sample in KB:
            b = True
            while b:
                try:
                    sample.remove(0)
                except:
                    b = False

        Supp = Supp.tolist()
        for sample in range(len(Supp)):
            Supp[sample] = Supp[sample].tolist()
            del_index = len(Supp[sample])
            for ts in range(len(Supp[sample])):
                b = True
                while b:
                    try:
                        Supp[sample][ts].remove(0)
                    except:
                        b = False
                if len(Supp[sample][ts]) == 0:
                    del_index = ts
                    break
            Supp[sample] = Supp[sample][:del_index]

        Outs = Outs.tolist()
        for sample in range(len(Outs)):
            Outs[sample] = Outs[sample].tolist()
            del_index = len(Outs[sample])
            for ts in range(len(Outs[sample])):
                b = True
                while b:
                    try:
                        Outs[sample][ts].remove(0)
                    except:
                        b = False
                if len(Outs[sample][ts]) == 0:
                    del_index = ts
                    break
            Outs[sample] = Outs[sample][:del_index]

        # get lengths
        ds1_kb_len = len(dataset_1['kB'])
        ds1_supp_len = len(dataset_1['supports'])
        ds1_outs_len = len(dataset_1['outputs'])

        ds2_kb_len = len(dataset_2['kB'])
        ds2_supp_len = len(dataset_2['supports'])
        ds2_outs_len = len(dataset_2['outputs'])

        # extract mooshed datasets
        actual_dataset_1_kb = KB[:ds1_kb_len]
        actual_dataset_1_supp = Supp[:ds1_supp_len]
        actual_dataset_1_outs = Outs[:ds1_outs_len]

        actual_dataset_2_kb = KB[ds1_kb_len : ds1_kb_len + ds2_kb_len]
        actual_dataset_2_supp = Supp[ds1_supp_len : ds1_supp_len + ds2_supp_len]
        actual_dataset_2_outs = Outs[ds1_outs_len : ds1_outs_len + ds2_outs_len]

        actual_dataset_3_kb = KB[ds1_kb_len + ds2_kb_len : ]
        actual_dataset_3_supp = Supp[ds1_supp_len + ds2_supp_len : ]
        actual_dataset_3_outs = Outs[ds1_outs_len + ds2_outs_len : ]

        # make sure they are equal with actual datasets
        self.assertEqual(dataset_1['kB'], actual_dataset_1_kb)
        self.assertEqual(dataset_1['supports'], actual_dataset_1_supp)
        self.assertEqual(dataset_1['outputs'], actual_dataset_1_outs)

        self.assertEqual(dataset_2['kB'], actual_dataset_2_kb)
        self.assertEqual(dataset_2['supports'], actual_dataset_2_supp)
        self.assertEqual(dataset_2['outputs'], actual_dataset_2_outs)

        self.assertEqual(dataset_3['kB'], actual_dataset_3_kb)
        self.assertEqual(dataset_3['supports'], actual_dataset_3_supp)
        self.assertEqual(dataset_3['outputs'], actual_dataset_3_outs)