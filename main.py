import tensorflow as tf
# gpu_devices = tf.config.experimental.list_physical_devices("GPU")
# for device in gpu_devices:
#     tf.config.experimental.set_memory_growth(device, True)
import os
from functools import partial
import random
import numpy
import io
import numpy as np
from models_layers import DeepModel, FlatModel
from dataset_manipulation import get_rdf_data, combine_datasets

# Tested
def precision(TP, FP):
    """Calculates precision from number of true positives and false positives."""
    return 0 if TP == 0 and FP == 0 else TP / (TP + FP)


# Tested
def recall(TP, FN):
    """Calculates recall using number of true positive and false negative."""
    return 0 if TP == 0 and FN == 0 else TP / (TP + FN)


# Tested
def F1(precision, recall):
    """Calculates F1 score from precision and recall numbers."""
    return 0 if precision == 0 and recall == 0 else 2 * (precision * recall) / (precision + recall)


def distance_Evaluations(log, shape, newPredictions, trueLabels):
    """Brings together and returns the results of multiple distance calculations. Currently is only returns custom."""
    custTN, custNT, countTrue, countNew, newEvalInfoCustDist = custom_distance(shape, newPredictions, trueLabels)

    log.write(
        "\nCustom Label Distance:\nCustom Distance From True to Predicted Data,{}\nCustom Distance From Predicted "
        "to True Data,{}".format(custTN, custNT))

    log.write(
        "\nAverage Custom Distance From True to Predicted Statement,{}\nAverage Custom Distance From Prediction to "
        "True Statement,{}\n".format(custTN / countTrue, 0 if countNew == 0 else custNT / countNew))

    c = write_evaluation_measures(newEvalInfoCustDist, log)

    return np.array(np.array([custTN, custNT, countTrue, countNew, c]))


def custom_distance(shape, newPred, trueLabels):
    """Finds the number of true and false positives and false negatives.  Also collects distance data which is recorded."""

    # Combines all the timestep lists together so can compare whole samples.
    flatTrue = [[item for sublist in x for item in sublist] for x in trueLabels]
    flatNew = [[item for sublist in x for item in sublist] for x in newPred]

    # [0] = True Positives, [1] = False Positives, [2] = False Negatives
    evalInfoNew = np.array([0, 0, 0])

    custTN = 0 # True to New
    custNT = 0

    countTrue = 0
    countNew = 0

    # Loops through the max size of an output tensor of the model.
    for sampleNum in range(shape[0]):
        for timestepNum in range(shape[1]):
            for tripleNum in range(shape[2]):

                # There is another true label to cal. dist.
                if len(trueLabels) > sampleNum and len(trueLabels[sampleNum]) > timestepNum and len(trueLabels[sampleNum][timestepNum]) > tripleNum:
                    countTrue = countTrue + 1

                    if len(flatNew) > sampleNum:
                        custTN = custTN + find_best_prediction_custom(trueLabels[sampleNum][timestepNum][tripleNum], flatNew[sampleNum])

                # There is another new prediction to cal. dist.
                if len(newPred) > sampleNum and len(newPred[sampleNum]) > timestepNum and len(newPred[sampleNum][timestepNum]) > tripleNum:
                    countNew = countNew + 1
                    if len(flatTrue) > sampleNum:
                        best = find_best_prediction_custom(newPred[sampleNum][timestepNum][tripleNum], flatTrue[sampleNum])
                        if best == 0:
                            evalInfoNew[0] = evalInfoNew[0] + 1
                        custNT = custNT + best

    # Calculating False positives.
    evalInfoNew[1] = countNew - evalInfoNew[0]
    evalInfoNew[2] = countTrue - evalInfoNew[0]
    return custTN, custNT, countTrue, countNew, evalInfoNew


def find_best_prediction_custom(statement, otherKB):
    if otherKB != []:
        return min(map(partial(custom, statement), otherKB))
    else:
        return custom(statement, [])


# Tested
def custom(tupleTriple1, tupleTriple2):
    if len(tupleTriple1) < len(tupleTriple2): return custom(tupleTriple2, tupleTriple1)

    dist = 0

    if tupleTriple1 == tupleTriple2:
        return 0
    else:
        for k in range(len(tupleTriple1)):
            string1 = tupleTriple1[k]
            string2 = tupleTriple2[k] if len(tupleTriple2) > k else ""
            if string2 == "":
                dist = dist + int(
                    ''.join(x for x in string1 if x.isdigit()))  # + (conceptSpace if string1[0] == 'C' else roleSpace)
            else:
                if (string1[0] == 'C' and string2[0] == 'R') or (string1[0] == 'R' and string2[0] == 'C'):
                    dist = dist + abs(
                        int(''.join(x for x in string1 if x.isdigit())) + int(''.join(x for x in string2 if x.isdigit())))
                else:
                    dist = dist + abs(
                        int(''.join(x for x in string1 if x.isdigit())) - int(''.join(x for x in string2 if x.isdigit())))

    return dist


# $$


def write_vector_file(filename, vector):
    file = io.open(filename, "w", encoding='utf-8')
    for i in range(len(vector)):
        file.write("Trial: {}\n".format(i))
        for j in range(len(vector[i])):
            file.write("\tStep: {}\n".format(j))
            for k in range(len(vector[i][j])):
                file.write("\t\t{}\n".format(vector[i][j][k]))
        file.write("\n")
    file.close()


# Tested
def training_stats(log, mseNew, mse0, mseL):
    """Calculates and logs training data passed from a learning model."""
    mseNew, mse0, mseL = mseNew.numpy(), mse0.numpy(), mseL.numpy()

    log.write(
        "Training Statistics\nPrediction Mean Squared Error,{}\nLearned Reduction MSE,{}\nIncrease (+) or Decrease (-) "
        "MSE on Test,{}\n" "Training Percent Change MSE,{}%\n".format(
            np.float32(mseNew), round(mse0 - mseL, 5), round(np.float32(mseNew) - mseL, 5), round(((mseL - mse0) / mse0 * 100)), 4))


# Tested
def write_evaluation_measures(F, log):
    TPs, FPs, FNs = F
    pre = precision(TPs, FPs)
    rec = recall(TPs, FNs)
    F = F1(pre, rec)

    log.write(
        "\nPrediction Accuracy For this Distance Measure\nTrue Positives,{}\nFalse Positives,{}\nFalse Negatives,{}\nPrecision,{}\nRecall,{}\nF1 Score,{}\n".format(
            TPs, FPs, FNs, pre, rec, F))

    x = np.array([TPs, FPs, FNs, pre, rec, F])

    return x


# Tested barely
def write_final_average_data(result, log):
    custTN, custNT, countTrue, countNew, (TPs1, FPs1, FNs1, pre1, rec1, F1) = result[0]

    log.write(
        "\nCustom Label Distance:\nAverage Custom Distance From True to Predicted Data,{}\nAverage Custom Distance "
        "From Predicted to True Data,{}".format(custTN, custNT))

    log.write(
        "\nAverage Custom Distance From True to Predicted Statement,{}\nAverage Custom Distance From Prediction to "
        "True Statement,{}\n".format(custTN / countTrue, custNT / countNew))

    log.write(
        "\nAverage Prediction Accuracy For this Distance Measure\nTrue Positives,{}\nFalse Positives,{}\nFalse "
        "Negatives,{}\nPrecision,{}\nRecall,{}\nF1 Score,{}\n".format(TPs1, FPs1, FNs1, pre1, rec1, F1))


# $$


# Tested
def cross_validation_split_all_data(n, KBs, supports, outputs):
    # Potentially calculates size of the 3D tensor which will be padded and passed to the LSTM.
    fileShapes1 = [len(supports[0]), len(max(supports, key=lambda coll: len(coll[0]))[0]),
                   len(max(outputs, key=lambda coll: len(coll[0]))[0])]

    print("Repeating KBs")
    # Creates a new 3D tensor where the original KB sample is copied once per timestep.
    newKBs = numpy.zeros([KBs.shape[0], fileShapes1[0], KBs.shape[1]], dtype=numpy.float32)
    # Goes through for every line of the KB
    for crossNum in range(len(newKBs)):
        for sampleNum in range(fileShapes1[0]):
            # The same KB line is copied timestep times in the sampleNum's place.
            newKBs[crossNum][sampleNum] = KBs[crossNum]

    KBs = newKBs

    print("Shuffling Split Indices")
    # Creates a list of test_indices up to the length of the # of Batch size?
    test_indices = list(range(len(KBs)))
    random.shuffle(test_indices)
    # k is the quotient and m is the remainder
    k, m = divmod(len(test_indices), n)
    # Creating num of cross validations of lists and assigning which test_indices are going to each one.
    test_indices = list(test_indices[i * k + min(i, m):(i + 1) * k + min(i + 1, m)] for i in range(n))

    # Creating place holding arrays of correct shape to be filled later.
    crossKBsTest = numpy.zeros((len(test_indices), len(test_indices[0]), len(KBs[0]), len(KBs[0][0])), dtype=numpy.float32)
    crossSupportsTest = numpy.zeros(
        (len(test_indices), len(test_indices[0]), len(supports[0]), fileShapes1[1]), dtype=numpy.float32)
    crossOutputsTest = numpy.zeros(
        (len(test_indices), len(test_indices[0]), len(outputs[0]), fileShapes1[2]), dtype=numpy.float32)

    # Probably don't need yet.
    # crossErrTest = None if not isinstance(mKBs, numpy.ndarray) else numpy.zeros(
    #     (len(test_indices), len(test_indices[0]), len(mouts[0]), maxout), dtype=float)

    print("Extracting Test Sets")
    for crossNum in range(len(test_indices)): # crossNum is each cross validation fold.
        KBns = []
        for sampleNum in range(len(test_indices[crossNum])): # sampleNum is each index in the crossNum validation fold.
            crossKBsTest[crossNum][sampleNum] = KBs[test_indices[crossNum][sampleNum]] # assigning whole (timestep x numbers) arrays

            # I think the hstack is padding each support and output with zeros to match the max len.
            crossSupportsTest[crossNum][sampleNum] = numpy.hstack([supports[test_indices[crossNum][sampleNum]], numpy.zeros(
                [fileShapes1[0], fileShapes1[1] - len(supports[test_indices[crossNum][sampleNum]][0])])])
            crossOutputsTest[crossNum][sampleNum] = numpy.hstack([outputs[test_indices[crossNum][sampleNum]], numpy.zeros(
                [fileShapes1[0], fileShapes1[2] - len(outputs[test_indices[crossNum][sampleNum]][0])])])

        write_vector_file("crossValidationFolds/output/originalKBsIn[{}].txt".format(crossNum), numpy.array(KBns))

    crossKBsTrain = numpy.zeros((n, len(KBs) - len(crossKBsTest[0]), len(KBs[0]), len(KBs[0][0])), dtype=numpy.float32)
    crossOutputsTrain = numpy.zeros((n, len(KBs) - len(crossKBsTest[0]), len(outputs[0]), fileShapes1[2]), dtype=numpy.float32)
    crossSupportsTrain = numpy.zeros((n, len(KBs) - len(crossKBsTest[0]), len(supports[0]), fileShapes1[1]), dtype=numpy.float32)

    print("Extracting Train Sets")
    for crossNum in range(len(test_indices)):

        all_indices = set(range(len(KBs)))
        trainIndices = list(all_indices.difference(set(test_indices[crossNum])))
        random.shuffle(trainIndices)

        for sampleNum in range(len(crossKBsTrain[crossNum])):

            crossKBsTrain[crossNum][sampleNum] = KBs[trainIndices[sampleNum]]

            crossSupportsTrain[crossNum][sampleNum] = numpy.hstack([supports[trainIndices[sampleNum]], numpy.zeros(
                [fileShapes1[0], fileShapes1[1] - len(supports[trainIndices[sampleNum]][0])])])

            crossOutputsTrain[crossNum][sampleNum] = numpy.hstack([outputs[trainIndices[sampleNum]],
                                                                   numpy.zeros([fileShapes1[0], fileShapes1[2] - len(
                                                                       outputs[trainIndices[sampleNum]][0])])])

    return crossKBsTest, crossKBsTrain, crossSupportsTrain, crossSupportsTest, crossOutputsTrain, crossOutputsTest


def get_labels_from_encoding(trueValues, predValues, decodeNonProperty, decodeProperty):
    """Creates and returns the label representation of a models true and predicted encoded array."""

    if type(predValues) is not numpy.ndarray:
        predValues = predValues.numpy()

    trueArray = np.zeros(shape=(trueValues.shape[0], trueValues.shape[1]), dtype=tuple)
    predArray = np.zeros(shape=(predValues.shape[0], predValues.shape[1]), dtype=tuple)

    for sampleNum in range(len(trueValues)):
        for timeStepNum in range(len(trueValues[sampleNum])):
            trueTriple = []
            predTriple = []
            timeStepListTripleTrue = []
            timeStepListTriplePred = []
            for item in range(len(trueValues[sampleNum][timeStepNum])):
                trueTriple.append(trueValues[sampleNum][timeStepNum][item])
                predTriple.append(predValues[sampleNum][timeStepNum][item])
                if len(trueTriple) == 3:
                    label1 = convert_encoded_float_to_label(trueTriple[0], decodeNonProperty, decodeProperty)
                    label2 = convert_encoded_float_to_label(trueTriple[1], decodeNonProperty, decodeProperty)
                    label3 = convert_encoded_float_to_label(trueTriple[2], decodeNonProperty, decodeProperty)
                    if label1 == '0' or label2 == '0' or label3 == '0':
                        pass
                    else:
                        timeStepListTripleTrue.append(tuple((label1, label2, label3)))
                    trueTriple = []
                if len(predTriple) == 3:
                    label1 = convert_encoded_float_to_label(predTriple[0], decodeNonProperty, decodeProperty)
                    label2 = convert_encoded_float_to_label(predTriple[1], decodeNonProperty, decodeProperty)
                    label3 = convert_encoded_float_to_label(predTriple[2], decodeNonProperty, decodeProperty)
                    if label1 == '0' or label2 == '0' or label3 == '0':
                        pass
                    else:
                        timeStepListTriplePred.append(tuple((label1, label2, label3)))
                    predTriple = []
            trueArray[sampleNum][timeStepNum] = timeStepListTripleTrue
            predArray[sampleNum][timeStepNum] = timeStepListTriplePred

    return trueArray, predArray


def convert_encoded_float_to_label(float, decodeNonProperty, decodeProperty):
    """Converts one floating encoded number to its appropriet label based on the decoding numbers."""
    if float > 0:
        labelNum = round(float * decodeNonProperty)
        if labelNum == 0:
            return '0'
        if labelNum > decodeNonProperty:
            labelNum = decodeNonProperty
        return 'C' + str(labelNum)
    elif float < 0:
        labelNum = round(float * decodeProperty) * -1
        if labelNum == 0:
            return '0'
        if labelNum > decodeProperty:
            labelNum = decodeProperty
        return 'R' + str(labelNum)
    else:
        return '0'


# $$


def flat_system(epochs, lr, data, trainlog, evallog):
    """"Trains and evaluates a base line flat LSTM for comparison with other rnn models."""
    KBs_test, KBs_train, X_train, X_test, y_train, y_test, decodeNonProperty, decodeProperty = data

    trainlog.write("Flat LSTM\nEpoch,Mean Squared Error,Root Mean Squared Error\n")
    evallog.write("\nFlat LSTM\n\n")
    print("")

    model = FlatModel(KBs_train.shape[1:0], num_outs=y_train.shape[2])
    optimizer = tf.keras.optimizers.Adam(learning_rate=lr)
    loss_fn = tf.keras.losses.MeanSquaredError()

    mse0 = 0
    mseL = 0
    for epoch in range(epochs):
        print("Flat System\t\tEpoch: {}".format(epoch))
        with tf.GradientTape() as tape:
            output = model(KBs_train)
            loss = loss_fn(y_train, output)
            trainlog.write("{},{}\n".format(epoch, loss))

        grads = tape.gradient(loss, model.trainable_weights)
        optimizer.apply_gradients(zip(grads, model.trainable_weights))

        if epoch == 0:
            mse0 = loss
        if epoch == epochs - 1:
            mseL = loss

    print("\nEvaluating Result\n")

    preds = model(KBs_test)
    loss = loss_fn(y_test, preds)

    training_stats(evallog, loss, mse0, mseL)

    evallog.write("\nTest Data Evaluation\n")

    trueLabels, labelPredictions = get_labels_from_encoding(y_test, preds, decodeNonProperty, decodeProperty)

    return distance_Evaluations(evallog, preds.shape, labelPredictions, trueLabels)


def deep_system(epochs, lr, data, trainlog, evallog):
    """Trains and evaluates a simple multilayer LSTM."""
    KBs_test, KBs_train, X_train, X_test, y_train, y_test, decodeNonProperty, decodeProperty = data

    trainlog.write("Deep LSTM\nEpoch,Mean Squared Error,Root Mean Squared Error\n")
    print("")

    model = DeepModel(KBs_train.shape[1:0], num_middle=X_train.shape[2], num_outs=y_train.shape[2])
    optimizer = tf.keras.optimizers.Adam(learning_rate=lr)
    loss_fn = tf.keras.losses.MeanSquaredError()

    mse0 = 0
    mseL = 0
    for epoch in range(epochs):
        print("Deep System\t\tEpoch: {}".format(epoch))
        with tf.GradientTape() as tape:
            output = model(KBs_train)
            loss = loss_fn(y_train, output)
            trainlog.write("{},{}\n".format(epoch, loss))

        grads = tape.gradient(loss, model.trainable_weights)
        optimizer.apply_gradients(zip(grads, model.trainable_weights))

        if epoch == 0:
            mse0 = loss
        if epoch == epochs - 1:
            mseL = loss

    print("\nEvaluating Result\n")

    preds = model(KBs_test)
    loss = loss_fn(y_test, preds)

    training_stats(evallog, loss, mse0, mseL)

    evallog.write("\nTest Data Evaluation\n")

    trueLabels, labelPredictions = get_labels_from_encoding(y_test, preds, decodeNonProperty, decodeProperty)

    return distance_Evaluations(evallog, preds.shape, labelPredictions, trueLabels)


def piecewise_system(epochs, lr, data, trainlog, evallog, cross_fold):
    """Trains and evaluates a system composed of two flat LSTM models which map from inputs to a support superset of the
    inputs.  It then maps from those supports to the output."""
    KBs_test, KBs_train, X_train, X_test, y_train, y_test, decodeNonProperty, decodeProperty = data

    trainlog.write("Piecewise LSTM Part One\nEpoch,Mean Squared Error,Root Mean Squared Error\n")
    evallog.write("Piecewise LSTM Part One\n")
    print("")

    model = FlatModel(input_shape=KBs_train.shape[1:0], num_outs=X_train.shape[2])
    optimizer = tf.keras.optimizers.Adam(learning_rate=lr)
    loss_fn = tf.keras.losses.MeanSquaredError()

    mse0 = 0
    mseL = 0
    for epoch in range(epochs):
        print("Piecewise System\t\tEpoch: {}".format(epoch))
        with tf.GradientTape() as tape:
            output = model(KBs_train)
            loss = loss_fn(X_train, output)
            trainlog.write("{},{}\n".format(epoch, loss))

        grads = tape.gradient(loss, model.trainable_weights)
        optimizer.apply_gradients(zip(grads, model.trainable_weights))

        if epoch == 0:
            mse0 = loss
        if epoch == epochs - 1:
            mseL = loss

    print("\nTesting first half\n")

    mid_preds = model(KBs_test)
    loss = loss_fn(X_test, mid_preds)

    training_stats(evallog, loss, mse0, mseL)

    numpy.savez("crossValidationFolds/saves/halfwayData[{}]".format(cross_fold), mid_preds)

    trainlog.write("Piecewise LSTM Part Two\nEpoch,Mean Squared Error,Root Mean Squared Error\n")
    evallog.write("\nPiecewise LSTM Part Two\n")

    model2 = FlatModel(X_train.shape[1:0], num_outs=y_train.shape[2])
    optimizer2 = tf.keras.optimizers.Adam(learning_rate=lr)
    loss_fn2 = tf.keras.losses.MeanSquaredError()

    mse0 = 0
    mseL = 0
    for epoch in range(epochs):
        print("Piecewise System\t\tEpoch: {}".format(epoch))
        with tf.GradientTape() as tape:
            output = model2(X_train)
            loss = loss_fn2(y_train, output)
            trainlog.write("{},{}\n".format(epoch, loss))

        grads = tape.gradient(loss, model2.trainable_weights)
        optimizer2.apply_gradients(zip(grads, model2.trainable_weights))

        if epoch == 0:
            mse0 = loss
        if epoch == epochs - 1:
            mseL = loss

    print("\nTesting second half")

    # Run model using true inputs to calculate loss.
    preds_final = model2(X_test)
    loss = loss_fn(y_test, preds_final)

    training_stats(evallog, loss, mse0, mseL)

    print("\nEvaluating Result")

    evallog.write("\nReasoner Support Test Data Evaluation\n")

    # Run model using outputs from previous model to observe actual results.
    preds_actual = model2(mid_preds)
    loss = loss_fn(y_test, preds_actual)

    evallog.write("\nSaved Test Data From Previous LSTM Evaluation\n")

    evallog.write("\nTesting Statistic\nIncrease MSE on Saved,{}\n".format(numpy.float32(loss) - mseL))

    trueLabels, labelPredictions = get_labels_from_encoding(y_test, preds_actual, decodeNonProperty, decodeProperty)

    return distance_Evaluations(evallog, preds_actual.shape, labelPredictions, trueLabels)


def run_system_cross_validation(system_type, cross_num, epochs, lr, dataFile):
    # Sets up logging.
    if not os.path.isdir("crossValidationFolds"): os.mkdir("crossValidationFolds")
    if not os.path.isdir("crossValidationFolds/training"): os.mkdir("crossValidationFolds/training")
    if not os.path.isdir("crossValidationFolds/evals"): os.mkdir("crossValidationFolds/evals")
    if not os.path.isdir("crossValidationFolds/saves"): os.mkdir("crossValidationFolds/saves")
    if not os.path.isdir("crossValidationFolds/output"): os.mkdir("crossValidationFolds/output")

    # Gets raw data.
    data = np.load(dataFile, allow_pickle=True)
    KB, supports, outputs, decode_data = data['arr_0'], data['arr_1'], data['arr_2'], data['arr_3']
    decodeNonProperty, decodeProperty = decode_data

    # Processes data.
    allTheData = cross_validation_split_all_data(cross_num, KB, supports, outputs)

    KBs_tests, KBs_trains, X_trains, X_tests, y_trains, y_tests = allTheData

    evals = numpy.zeros((1, 5), dtype=numpy.float64)
    for i in range(cross_num):
        print("\nCross Validation Fold {}\n".format(i))

        training_log = open("crossValidationFolds/training/trainFold[{}].csv".format(i), "w")
        eval_log = open("crossValidationFolds/evals/evalFold[{}].csv".format(i), "w")

        if system_type == 'Flat_System':
            evals = evals + flat_system(epochs, lr, (KBs_tests[i], KBs_trains[i], X_trains[i], X_tests[i], y_trains[i],
                                                 y_tests[i], decodeNonProperty, decodeProperty), training_log, eval_log)

        if system_type == 'Deep_System':
            evals = evals + deep_system(epochs, lr, (KBs_tests[i], KBs_trains[i], X_trains[i], X_tests[i], y_trains[i],
                                                 y_tests[i], decodeNonProperty, decodeProperty), training_log, eval_log)

        if system_type == 'Piece_System':
            evals = evals + piecewise_system(epochs, lr, (KBs_tests[i], KBs_trains[i], X_trains[i], X_tests[i], y_trains[i],
                                             y_tests[i], decodeNonProperty, decodeProperty), training_log, eval_log, i)

    evals = evals / cross_num

    print("Summarizing Results")

    log = open("crossValidationFolds/AvgEvaluation_{}.csv".format(system_type), "w")

    log.write("\n{}\n".format(system_type))

    write_final_average_data(evals, log)

    log.close()

    print("\nDone")

    return evals


if __name__ == "__main__":
    data = ['C:\\Users\\Brayden Pankaskie\\Desktop\\LabStuff\\RDFSData\\dbpedia_2015_8.json']

    KB, Supp, Outs, decode_nonproperty, decode_property = combine_datasets(data)

    np.savez_compressed('data/easy_training.npz', KB, Supp, Outs, [decode_nonproperty, decode_property])

    run_system_cross_validation(system_type='Deep_System', cross_num=3, epochs=1000, lr=.01,
                                dataFile='data/easy_training.npz')